package mx.com.juan.clima

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity

class NetWork {
    companion object{
        fun statered(activity: AppCompatActivity):Boolean{
            val connectManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val networkInfo = connectManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
    }
}