package mx.com.juan.clima

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var txtTemperature: TextView? = null
    var txtCuidad: TextView? = null
    var txtEstatus: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        txtTemperature = findViewById(R.id.txtTemperature)
        txtCuidad = findViewById(R.id.txtCuidad)
        txtEstatus = findViewById(R.id.txtStatus)
        val cuidad = clima("Mexico", "21", "Soleado")
        txtTemperature?.text = cuidad.grados.toString() + "°C"
        txtCuidad?.text = cuidad.nombre.toString()
        txtEstatus?.text = cuidad.estatus.toString()
if (NetWork.statered(this)){
    Toast.makeText(this,"Hay internet",Toast.LENGTH_SHORT).show()
}else{

    Toast.makeText(this,"no hay conexion de red",Toast.LENGTH_SHORT).show()
}
    }

}
